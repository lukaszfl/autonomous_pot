ESP-IDF AUTONOMOUS_POT
========================
This is a projects that control a pot using an esp32

HOW TO INSTALL
========================
First you need to configure [esp-idf](https://github.com/espressif/esp-idf), toolchains, environment paths etc.

Next step is cloning this repo to your esp directory.

To flash it into esp chip use `make flash`. You can also add `monitor` to see in console if everything initializes correcttly.

Usage
========================
To set ssid, password, server url, db_login and db_password you can edit #defines in the main file or run

'make menuconfig' 

and select Example Configuration. After that it is possible to run by 'make flash'.



Disclaimer
=======================
There may be some errors or warnings due to that project was force-made into c++.
