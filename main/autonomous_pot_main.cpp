/* This example code is in the Public Domain (or CC0 licensed, at your option.)
   Unless required by applicable law or agreed to in writing, this software is 
   distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR  
   CONDITIONS OF ANY KIND, either express or implied.
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <string>
#include <sstream>
#include <iomanip>
#include <iostream>
#include <stdio.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/event_groups.h"
#include "esp_system.h"
#include "esp_wifi.h"
#include "esp_event_loop.h"
#include "esp_log.h"
#include "nvs_flash.h"

#include "lwip/err.h"
#include "lwip/sys.h" 

#include "esp_http_client.h"

#include "driver/gpio.h"
#include "driver/adc.h"
#include "driver/rtc_io.h"
#include "esp_adc_cal.h"

#include "math.h"

extern "C" {
  void app_main(void);
}


#include "DataHandler.h"

/** 
 * Note:
 * 1. Win10 does not support vendor report , So SUPPORT_REPORT_VENDOR is always set to FALSE, it defines in hidd_le_prf_int.h
 * 2. Update connection parameters are not allowed during iPhone HID encryption, slave turns 
 * off the ability to automatically update connection parameters during encryption.
 * 3. After our HID device is connected, the iPhones write 1 to the Report Characteristic Configuration Descriptor, 
 * even if the HID encryption is not completed. This should actually be written 1 after the HID encryption is completed.
 * we modify the permissions of the Report Characteristic Configuration Descriptor to `ESP_GATT_PERM_READ | ESP_GATT_PERM_WRITE_ENCRYPTED`.
 * if you got `GATT_INSUF_ENCRYPTION` error, please ignore.
 */


// TODO: later on change that ti get from config file
// #define WIFI_SSID      "WozObserwacyjnyCBA_42"
// #define WIFI_SSID_PASSWORD      "rtxXaedwQhx3"
#define WIFI_SSID             CONFIG_WIFI_SSID
#define WIFI_SSID_PASSWORD    CONFIG_WIFI_SSID_PASSWORD
#define WIFI_MAXIMUM_RETRY    CONFIG_WIFI_MAXIMUM_RETRY
#define WIFI_MAC              CONFIG_WIFI_MAC

#define DB_REQUEST_URL        CONFIG_DB_REQUEST_URL
#define DB_USER_LOGIN         CONFIG_DB_USER_LOGIN
#define DB_USER_PASSWORD      CONFIG_DB_USER_PASSWORD

#define SYSTEM_SLEEP_DURATION_SEC     CONFIG_SYSTEM_SLEEP_DURATION_SEC
#define SYSTEM_DESIRED_HUMIDITY     CONFIG_SYSTEM_DESIRED_HUMIDITY
#define SYSTEM_MINIMUM_WATER    10.0 // don't want to dry-pump
/* FreeRTOS event group to signal when we are connected*/
static EventGroupHandle_t s_wifi_event_group;


/* The event group allows multiple bits for each event, but we only care about one event 
 * - are we connected to the AP with an IP? */
const int WIFI_CONNECTED_BIT = BIT0;

static int s_retry_num = 0;


#define SYSTEM_TAG  "SYSTEM"
#define GPIO_TAG    "GPIO"
#define WIFI_TAG    "WIFI"
#define ADC_TAG     "ADC"


/*
GPIO6-11 are usually used for SPI flash.
GPIO20, GPIO24, and GPIO28-31 are not available as pins.
GPIO34-39 can only be set as input mode and do not have software pullup or pulldown functions.
*/

#define GPIO_PUMP_ENABLE      15
// #define GPIO_SENSORS_ENABLE   24
#define GPIO_OUTPUT_PIN_SEL   (1ULL<<GPIO_PUMP_ENABLE)


#define ADC_MAX_VAL          4096
#define ADC_VREF             3300.0         //Use adc2_vref_to_gpio() to obtain a better estimate
#define ADC_NO_OF_SAMPLES    64             //Multisampling
#define TEMPERATURE_V_OFFSET 500

static esp_adc_cal_characteristics_t *adc_chars;
static const adc_channel_t ADC_HUMIDITY_CHANNEL = ADC_CHANNEL_3;     // GPIO39
static const adc_channel_t ADC_LUMINOSITY_CHANNEL = ADC_CHANNEL_6;     // GPIO34
static const adc_channel_t ADC_TEMPERATURE_CHANNEL = ADC_CHANNEL_0;     // GPIO36
static const adc_channel_t ADC_WATER_LEVEL_CHANNEL = ADC_CHANNEL_7;     // GPIO35
static const adc_atten_t ADC_ATTEN = ADC_ATTEN_DB_0;  // no attenuation
static const adc_unit_t ADC_UNIT = ADC_UNIT_1; // ADC1


static const double WATER_LEVEL_VIN = 4.2; // voltage given as input to the water tank potentiometer 
static const double WATER_LEVEL_L = 22.0; // length of the lever arm in cm
static const double WATER_LEVEL_H_MAX = 20.0; // maximum height of water in tank in cm
static const double WATER_LEVEL_ALFA_MAX = 300.0; // maximum angle that potentiometer can go in degrees

static const double TMP36_OFFSET = 500; // mV

std::string uint8_mac_to_hex_string(const uint8_t *v, const size_t s) {
  std::stringstream ss;

  ss << std::hex << std::setfill('0');

  int i = 0;
  ss << std::hex << std::setw(2) << static_cast<int>(v[i]);
  while(i < s)
  {
    ss << ":" << std::hex << std::setw(2) << static_cast<int>(v[++i]);
  }

  return ss.str();
}

void hex_string_mac_to_uint8(std::string const& in, uint8_t *mac_u) {
  unsigned int out[6];
    if (sscanf(in.c_str(),
                    "%02x:%02x:%02x:%02x:%02x:%02x",
                    &out[0], &out[1], &out[2],
                    &out[3], &out[4], &out[5]) != 6)
    {
        ESP_LOGI(WIFI_TAG, "Incorrect MAC address!");
    }

    mac_u[0] = (uint8_t)out[0];
    mac_u[1] = (uint8_t)out[1];
    mac_u[2] = (uint8_t)out[2];
    mac_u[3] = (uint8_t)out[3];
    mac_u[4] = (uint8_t)out[4];
    mac_u[5] = (uint8_t)out[5];
}

// initialize datahandler values
std::string DataHandler::email = DB_USER_LOGIN;
std::string DataHandler::password = DB_USER_PASSWORD;
std::string DataHandler::request_url = DB_REQUEST_URL;
std::string DataHandler::mac = WIFI_MAC;
double DataHandler::humidity = 0.0;
double DataHandler::luminosity = 0.0;
double DataHandler::temperature = 0.0;
double DataHandler::water_level = 0.0;
bool DataHandler::watered = false;



static esp_err_t event_handler(void *ctx, system_event_t *event)
{
  switch(event->event_id) {
  case SYSTEM_EVENT_STA_START:
    esp_wifi_connect();
    break;
  case SYSTEM_EVENT_STA_GOT_IP:
    ESP_LOGI(WIFI_TAG, "got ip:%s", ip4addr_ntoa(&event->event_info.got_ip.ip_info.ip));
    s_retry_num = 0;
    xEventGroupSetBits(s_wifi_event_group, WIFI_CONNECTED_BIT);
    break;
  case SYSTEM_EVENT_STA_DISCONNECTED:
  {
    if (s_retry_num < WIFI_MAXIMUM_RETRY) {
      esp_wifi_connect();
      xEventGroupClearBits(s_wifi_event_group, WIFI_CONNECTED_BIT);
      s_retry_num++;
      ESP_LOGI(WIFI_TAG,"retry to connect to the AP");
    }
    ESP_LOGI(WIFI_TAG,"connect to the AP fail\n");
    break;
    }
  default:
    break;
  }
  return ESP_OK;
}

void wifi_init_sta()
{
  s_wifi_event_group = xEventGroupCreate();

  tcpip_adapter_init();
  ESP_ERROR_CHECK(esp_event_loop_init(event_handler, NULL) );

  wifi_init_config_t cfg = WIFI_INIT_CONFIG_DEFAULT();
  ESP_ERROR_CHECK(esp_wifi_init(&cfg));
  wifi_sta_config_t wifi_sta_config;
  // little trick used here to get it going in cpp
  memmove(wifi_sta_config.ssid, WIFI_SSID, 32);
  memmove(wifi_sta_config.password, WIFI_SSID_PASSWORD, 64);
  // wifi_sta_config.ssid = WIFI_SSID;
  // wifi_sta_config.password = WIFI_SSID_PASSWORD;
  wifi_config_t wifi_config;
  wifi_config.sta = wifi_sta_config;
  // wifi_config_t wifi_config = {
  //   .sta = {
  //     .ssid = WIFI_SSID,
  //     .password = WIFI_SSID_PASSWORD
  //   },
  // };

  ESP_ERROR_CHECK(esp_wifi_set_mode(WIFI_MODE_STA));
  ESP_ERROR_CHECK(esp_wifi_set_config(ESP_IF_WIFI_STA, &wifi_config));
  ESP_ERROR_CHECK(esp_wifi_start());

  uint8_t mac[] = {0, 0, 0, 0, 0, 0}; 
  hex_string_mac_to_uint8(WIFI_MAC, mac);
  ESP_ERROR_CHECK(esp_wifi_set_mac(ESP_IF_WIFI_STA, mac));



  ESP_LOGI(WIFI_TAG, "Init complete!");
  ESP_LOGI(WIFI_TAG, "connect to ap SSID:%s password:%s", WIFI_SSID, WIFI_SSID_PASSWORD);
}

void adc_init()
{
  adc1_config_width(ADC_WIDTH_BIT_12);
  adc1_config_channel_atten((adc1_channel_t)ADC_HUMIDITY_CHANNEL, ADC_ATTEN);
  adc1_config_channel_atten((adc1_channel_t)ADC_LUMINOSITY_CHANNEL, ADC_ATTEN);
  adc1_config_channel_atten((adc1_channel_t)ADC_TEMPERATURE_CHANNEL, ADC_ATTEN);
  adc1_config_channel_atten((adc1_channel_t)ADC_WATER_LEVEL_CHANNEL, ADC_ATTEN);


  //Characterize ADC
  adc_chars = (esp_adc_cal_characteristics_t*)calloc(1, sizeof(esp_adc_cal_characteristics_t));
  esp_adc_cal_value_t val_type = esp_adc_cal_characterize(ADC_UNIT, ADC_ATTEN, ADC_WIDTH_BIT_12, ADC_VREF, adc_chars);

  ESP_LOGI(ADC_TAG, "Init complete!");
}

double read_adc_mvolts(adc_channel_t channel)
{
  uint32_t adc_reading = 0;
  //Multisampling
  for (int i = 0; i < ADC_NO_OF_SAMPLES; i++) {
    adc_reading += adc1_get_raw((adc1_channel_t)channel);
  }
  adc_reading /= ADC_NO_OF_SAMPLES;
  //Convert adc_reading to voltage in mV
  // uint32_t voltage = esp_adc_cal_raw_to_voltage(adc_reading, adc_chars);
  double voltage = (double)adc_reading / ADC_MAX_VAL * ADC_VREF;
  ESP_LOGI(ADC_TAG, "Raw: %d\tVoltage: %fmV\n", adc_reading, voltage);
  vTaskDelay(pdMS_TO_TICKS(1000));
  return (double)voltage;
}

void gpio_init()
{
    gpio_config_t io_conf;
    //disable interrupt
    io_conf.intr_type = (gpio_int_type_t)GPIO_PIN_INTR_DISABLE;
    //set as output mode
    io_conf.mode = GPIO_MODE_OUTPUT;
    //bit mask of the pins that you want to set,e.g.GPIO18/19
    io_conf.pin_bit_mask = GPIO_OUTPUT_PIN_SEL;
    //disable pull-down mode
    io_conf.pull_down_en = (gpio_pulldown_t)0;
    //disable pull-up mode
    io_conf.pull_up_en = (gpio_pullup_t)0;
    //configure GPIO with the given settings
    gpio_config(&io_conf);
    // rtc_gpio_init((gpio_num_t)GPIO_PUMP_ENABLE);
    // rtc_gpio_set_direction((gpio_num_t)GPIO_PUMP_ENABLE, RTC_GPIO_MODE_OUTPUT_ONLY);
    gpio_set_level((gpio_num_t)GPIO_PUMP_ENABLE, 0);

    // rtc_gpio_init((gpio_num_t)GPIO_SENSORS_ENABLE);
    // rtc_gpio_set_direction((gpio_num_t)GPIO_SENSORS_ENABLE, RTC_GPIO_MODE_OUTPUT_ONLY);
    // gpio_set_level((gpio_num_t)GPIO_SENSORS_ENABLE, 0);

    ESP_LOGI(GPIO_TAG, "Init complete!");
}

double get_water_level(double v)
{
  // get current angle of potentiometer
  double alfa = v * WATER_LEVEL_ALFA_MAX / WATER_LEVEL_VIN; // in degrees
  // get height of water in the tank
  double h = WATER_LEVEL_L * (1 - cos(M_PI * alfa / 180.0)); // in cm
  h = h > WATER_LEVEL_H_MAX ? WATER_LEVEL_H_MAX : h; // set boundries

  return h / WATER_LEVEL_H_MAX * 100.0; // return percents

}

bool water_plants()
{
  double humidity = read_adc_mvolts(ADC_HUMIDITY_CHANNEL) / ADC_VREF * 100.0;
  double water_level = get_water_level(read_adc_mvolts(ADC_WATER_LEVEL_CHANNEL));
  // check humidity level
  if( humidity >= SYSTEM_DESIRED_HUMIDITY || water_level < SYSTEM_MINIMUM_WATER)
  {
    ESP_LOGI(SYSTEM_TAG, "Too little water or enough moisture\thumidity: %f\twater_level: %f", humidity, water_level);
    return false;
  }
  // enable pump
  gpio_set_level((gpio_num_t)GPIO_PUMP_ENABLE, 1);

  while(humidity < SYSTEM_DESIRED_HUMIDITY && water_level >= SYSTEM_MINIMUM_WATER)
  {
    humidity = read_adc_mvolts(ADC_HUMIDITY_CHANNEL) / ADC_VREF * 100.0;
    water_level = get_water_level(read_adc_mvolts(ADC_WATER_LEVEL_CHANNEL));

    ESP_LOGI(SYSTEM_TAG, "watering...\thumidity: %f\twater_level: %f", humidity, water_level);
    vTaskDelay(1000 / portTICK_RATE_MS); // give water some time to flow
  }

  // disable pump
  gpio_set_level((gpio_num_t)GPIO_PUMP_ENABLE, 0);

  return true;
}


void app_main(void)
{
  //Initialize NVS
  esp_err_t ret = nvs_flash_init();
  if (ret == ESP_ERR_NVS_NO_FREE_PAGES || ret == ESP_ERR_NVS_NEW_VERSION_FOUND) {
    ESP_ERROR_CHECK(nvs_flash_erase());
    ret = nvs_flash_init();
  }
  ESP_ERROR_CHECK(ret);

  // initialize gpio
  gpio_init();

  // initialize adc
  adc_init();
  
  // power-up sensors
  // gpio_set_level((gpio_num_t)GPIO_SENSORS_ENABLE, 1);
  
  // gather data
  DataHandler::humidity = read_adc_mvolts(ADC_HUMIDITY_CHANNEL) / ADC_VREF * 100.0; // in percent
  DataHandler::luminosity = read_adc_mvolts(ADC_LUMINOSITY_CHANNEL) / ADC_VREF * 100.0; // in percent
  DataHandler::temperature = (read_adc_mvolts(ADC_TEMPERATURE_CHANNEL) - TMP36_OFFSET) / 100.0; // C degrees
  // DataHandler::water_level = read_adc_mvolts(ADC_WATER_LEVEL_CHANNEL) / ADC_VREF * 100.0; // in percent
  DataHandler::water_level = get_water_level(read_adc_mvolts(ADC_WATER_LEVEL_CHANNEL)); // in percent
  DataHandler::watered = water_plants(); // sensors must be already working for this

  // power-down sensors
  // gpio_set_level((gpio_num_t)GPIO_SENSORS_ENABLE, 0);
  
  // initialize wifi
  ESP_LOGI(WIFI_TAG, "ESP_WIFI_MODE_STA");
  wifi_init_sta();
  // make sure that there is a connection
  xEventGroupWaitBits(s_wifi_event_group, WIFI_CONNECTED_BIT, false, true, 2000);

  // try to send data to server
  DataHandler::send_data();
  
  // disable wifi
  ESP_ERROR_CHECK(esp_wifi_stop());

  // keep output pins levels during sleep mode
  // esp_sleep_pd_config(ESP_PD_DOMAIN_RTC_PERIPH, ESP_PD_OPTION_ON);
  gpio_deep_sleep_hold_en();
  // initialize sleep mode
  esp_sleep_enable_timer_wakeup(SYSTEM_SLEEP_DURATION_SEC * 1000000);
  // go to sleep
  esp_deep_sleep_start();
}

