#ifndef DATAHANDLER_H
#define DATAHANDLER_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <string>
#include <sstream>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/event_groups.h"
#include "esp_system.h"
#include "esp_wifi.h"
#include "esp_event_loop.h"
#include "esp_log.h"
#include "nvs_flash.h"

#include "lwip/err.h"
#include "lwip/sys.h" 

#include "esp_http_client.h"

#define DATA_TAG    "DATA"




namespace patch
{
  template < typename T > std::string to_string( const T& n)
  {
    std::ostringstream stm ;
    stm << n ;
    return stm.str() ;
  }
}

class DataHandler
{
public:
  static std::string email;
  static std::string password;
  static std::string request_url;
  static std::string mac;
  static double humidity; // 0-100
  static double luminosity; // 0-100
  static double temperature; // whatever
  static double water_level; // 0-100
  static bool watered;

  static std::string get_request_string();

  static bool send_data();

};

#endif