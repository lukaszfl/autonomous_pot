#include "DataHandler.h"

std::string DataHandler::get_request_string()
{
  std::string request = "email=";
  request += email;
  request += "&password=";
  request += password;
  request += "&mac=";
  request += mac;
  request += "&humidity=";
  request += patch::to_string(humidity);
  request += "&luminosity=";
  request += patch::to_string(luminosity);
  request += "&temperature=";
  request += patch::to_string(temperature);
  request += "&water_level=";
  request += patch::to_string(water_level);
  request += "&watered=";
  request += patch::to_string(watered);

  return request;
}

bool DataHandler::send_data()
{
  // prepare request
  esp_http_client_config_t config = {request_url.c_str()};
  // config.event_handler = _http_event_handler;
  //  = {
  //   .url = WIFI_REQUEST_URL,
  //   .event_handler = _http_event_handler,
  // };

  esp_http_client_handle_t client = esp_http_client_init(&config);
	esp_http_client_set_method(client, HTTP_METHOD_POST);
	esp_http_client_set_header(client, "Content-Type", "application/x-www-form-urlencoded");

  std::string request = get_request_string();

  ESP_LOGI(DATA_TAG, "request: %s", request.c_str());
	esp_http_client_set_post_field(client, request.c_str(), request.length());

  // send request
	esp_err_t err = esp_http_client_perform(client);
	if (err == ESP_OK) {
		ESP_LOGI(DATA_TAG, "HTTP POST Status = %d, content_length = %d",
			esp_http_client_get_status_code(client),
			esp_http_client_get_content_length(client));
      return true;
	}
	else {
		ESP_LOGE(DATA_TAG, "HTTP POST request failed: %s", esp_err_to_name(err));
    return false;
	}
}
